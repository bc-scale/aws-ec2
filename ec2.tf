data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}

resource "tls_private_key" "rsa-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ec2-key" {
  key_name   = var.ec2-key_name
  public_key = tls_private_key.rsa-key.public_key_openssh

  provisioner "local-exec" {
    command = <<-EOT
      echo '${tls_private_key.rsa-key.private_key_pem}' > ./'${var.ec2-key_name}'.pem
      chmod 400 ./'${var.ec2-key_name}'.pem
    EOT
  }
}

resource "aws_instance" "ec2-amazon-linux-2" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = var.instance_type
  key_name      = var.ec2-key_name
  depends_on = [
    aws_key_pair.ec2-key,
  ]

  tags = {
    Name     = "EC2 provisioned by Terraform"
    Owner    = "brunolabs"
    ManageBy = "Terraform"
  }
}